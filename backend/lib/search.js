var request                 = require('request');
var async                   = require('async');
var constants               = require('../config/constants');

module.exports = {
    /**
     * gets from API and searches flights
     * @param {object} query
     */
    findFlights: query => {
        var url = query.origin === constants.origin.from
            ? constants.searchEndpoints.arrivals
            : constants.searchEndpoints.departures;

        return new Promise((resolve, reject) => {
            async.waterfall([
                (callback) => {
                    request(url, (error, response, body) => {
                        if (error) return callback(error);
                        return callback(null, body);
                    });
                },
                (data, callback) => {
                    var result = [];

                    data = JSON.parse(data);

                    data.flights.forEach((flight, index) => {
                        // since one of the properties is always going to be Dubai
                        if ((flight.lang.en.originName.toLowerCase() === query.city
                            || flight.lang.en.destinationName.toLowerCase() === query.city)
                            && query.date.unix() >= flight.scheduled) {
                            if (query.carrier) {
                                if(flight.lang.en.airlineName.toLowerCase() === query.carrier)
                                    result.push(flight);
                            } else {
                                result.push(flight);
                            }
                        }
                    });

                    return callback(null, result);
                }
            ], (err, result) => {
                if (err) return reject(err);

                return resolve(result);
            });
        })
    }
};