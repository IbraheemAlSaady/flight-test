var redis               = require('redis');
var config              = require('../config');

client = redis.createClient();

module.exports = {
    cache: (key, value) => {
        key = key.toLowerCase().trim().replace(/' '/g, '-');
        client.set(key, JSON.stringify(value), 'EX', config.app.redisCacheTime);
    },
    get: key => {
        return new Promise((resolve, reject) => {
            key = key.toLowerCase().trim().replace(/' '/g, '-');
            client.get(key, function (err, value) {
                if(err) return reject(err);

                return resolve(JSON.parse(value));
            });
        });
    },
    getSync: key => {
        key = key.toLowerCase().trim().replace(/' '/g, '-');
        return client.get(key);
    }
};