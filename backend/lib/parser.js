var moment              = require('moment');
var constants               = require('../config/constants');

var basic = ['from', 'to'];
var carriers = ['flydubai', 'emirates'];
var cities = ['beirut', 'london', 'budapest', 'riyadh', 'jeddah', 'muscat'];
var dayNames = moment.weekdays().map(day => day.toLowerCase());
var dateWords = [
    'today', 'tomorrow', 'weekend'
];

/**
 * gets the origin
 * @param {string} text 
 */
function getOrigin(text) {
    var origin = basic.filter(word => {
        return text.toLowerCase().includes(word);
    })[0];

    if (!origin) throw new Error('no origin found');

    return constants.origin[origin];
};

/**
 * gets the search date
 * @param {string} text 
 */
function getDate(text) {
    var searchDate = null;

    var dateWord = dateWords.filter(word => {
        return text.toLowerCase().includes(word);
    })[0];

    // if dateWord has a value get the date
    if (dateWord) {
        switch (dateWords.indexOf(dateWord)) {
            case 0:
                searchDate = moment();
                break;
            case 1:
                searchDate = moment().add(1, 'days');
                break;
            case 2:
                searchDate = moment().day(5); // 5 means Friday
                break;
            default:
                searchDate = moment();
        }
    }
    else {
        var dayName = dayNames.filter(day => {
            return text.toLowerCase().includes(day);
        })[0];

        // if there is no dateWord or day name, set the searchDate to today
        if(! dayName) searchDate = moment();
        else {
            var dayINeed = dayNames.indexOf(dayName);

            if (moment().isoWeekday() < dayINeed) { 
                // then just give me this week's instance of that day
                searchDate = moment().isoWeekday(dayINeed);
            } else {
                // otherwise, give me next week's instance of that day
                searchDate = moment().add(1, 'weeks').isoWeekday(dayINeed);
              }
        }
    }

    return searchDate;
};

/**
 * Find from/to city
 * @param {string} text 
 */
function getCity(text) {
    var city = cities.filter(city => {
        return text.toLowerCase().includes(city);
    })[0];

    if(! city) throw new Error('City origin/desitination not found');

    return city;
};

/**
 * Find carier
 * @param {string} text 
 */
function getCarrier(text) {
    var carrier = carriers.filter(carrier => {
        return text.toLowerCase().includes(carrier);
    })[0];

    return carrier;
};

module.exports = {
    /**
     * a function to parse the search text sent by the user
     * @param {string} searchText
     */
    parse: (searchText) => {
        return new Promise((resolve, reject) => {
            try {
                var queryDate = {
                    origin: getOrigin(searchText),
                    date: getDate(searchText),
                    city: getCity(searchText),
                    carrier: getCarrier(searchText)
                };
                resolve(queryDate);
            } catch(error) {
                reject(error);
            }
        });
    }
}