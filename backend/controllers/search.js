var async               = require('async');
var parser              = require('../lib/parser');
var search              = require('../lib/search');
var cache               = require('../lib/cache');

var searchController = module.exports;

/**
 * setting up a controller
 * @param {any} app
 * @param {any} options
 */
searchController.setup = (app, options) => {
    app.get('/search', (req, res, next) => {
        var searchText = req.query.text;

        cache.get(searchText).then(result => {
            if(result) return res.send(result);
            else {
                async.waterfall([
                    (callback) => {
                        parser.parse(searchText).then(query => {
                            return callback(null, query);
                        }).catch(callback);
                    },
                    (query, callback) => {
                        search.findFlights(query).then(result => {
                            return callback(null, result);
                        }).catch(callback);
                    },
                    (result, callback) => {
                        cache.cache(searchText, result);
                        return callback(null, result);
                    }
                ], (err, result) => {
                    if(err) return next(err);
        
                    return res.send(result);
                });
            }
        }).catch(next);
    });
};