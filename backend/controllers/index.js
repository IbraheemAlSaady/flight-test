var parser              = require('../lib/parser');
var controllers = [
    require('./search')
];

var endpoints = module.exports;

endpoints.setup = (app, options) => {
    controllers.forEach(controller => controller.setup(app, options));
};