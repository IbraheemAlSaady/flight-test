# How to run the node app
Running the node server app

## Getting Started
the project by default will run on port 9900, the project is setup to be used with .env, so you can change the port in the .env file.

`node 6.9.1` is the node version, if you have nvm, then you can just do `nvm use` on the project root 

```sh
nvm use
npm install
npm start
```