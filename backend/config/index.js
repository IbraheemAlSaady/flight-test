require('dotenv').config();

var config = module.exports;

config.app = {
    port: process.env.PORT || 9900,
    redisCacheTime: process.env.CACHE || 60
};