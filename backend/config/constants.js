module.exports = {
    origin: {
        from: 0,
        to: 1
    },
    searchEndpoints: {
        departures: 'http://www.dubaiairports.ae/api/flight/departures?limit=100',
        arrivals: 'http://www.dubaiairports.ae/api/flight/arrivals?limit=100'
    }
};