var express             = require('express');
var cors                = require('cors')
var config              = require('./config');
var controllers         = require('./controllers');


var app = express();

app.use(cors());

controllers.setup(app);

app.listen(config.app.port, () => {
    console.log(`app listening to port ${config.app.port}`);
});