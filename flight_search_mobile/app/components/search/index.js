import React, { Component } from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { Container, Header, Content, Item, Input, Icon, Button, Text, Toast, View } from 'native-base';
import moment from 'moment';
import { get } from '../../services/api';

import Style from './style';

class SearchPage extends Component {
    constructor() {
        super();
        this.state = {
            searchText: 'flights to riyadh tomorrow',
            flights: [],
            fetching: false,
            error: null
        };
    }

    toast(message, duration = 3000) {
        Toast.show({
            text: message,
            duration,
            position: 'bottom',
        });
    }

    onSearchPressed() {
        if (!this.state.searchText) return this.toast('type something in the search field');

        else {
            this.setState({
                fetching: true
            }, () => {
                get(this.state.searchText).then(result => {
                    console.log(result);
                    this.setState({
                        flights: result,
                        fetching: false
                    });
                }).catch(err => {
                    console.log(err);
                    this.setState({
                        flights: [],
                        fetching: false,
                        error: 'something went wrong, try again..'
                    })
                });
            });

        }
    }

    renderFlightItem(item) {
        return (
            <View style={Style.flightItemContainer}>
                <Text style={Style.airlineName}>{item.fullName} ({item.airlineCode})</Text>
                <View style={Style.destinationContainer}>
                    <Text style={Style.city}>{item.lang.en.originName}</Text>
                    <Text style={Style.city}>TO</Text>
                    <Text style={Style.city}>{item.lang.en.destinationName}</Text>
                </View>
                <Text style={Style.schedule}>{moment.unix(item.scheduled).format("MMM DD, YYYY HH:mm")}</Text>
            </View>
        )
    }

    renderContent() {
        if (!this.state.flights.length && ! this.state.fetching && ! this.state.error) {
            return <Text style={Style.emptyMessage}>Search for flights...</Text>;
        }
        else if (this.state.fetching) return <ActivityIndicator animating={true} />
        else if(this.state.flights.length && !this.state.fetching)
            return (<FlatList
                style={Style.flightsList}
                data={this.state.flights}
                keyExtractor={(item) => item.uid}
                renderItem={({ item }) => this.renderFlightItem(item)}
                ItemSeparatorComponent={() => <View style={Style.separator}></View>}
            />);
        else if(this.state.error) return <Text style={Style.emptyMessage}>{this.state.error}</Text>;

    }

    render() {
        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input
                            style={Style.searchInput}
                            placeholder="Search flights..."
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ searchText: text })}
                            value={this.state.searchText} />
                    </Item>
                    <Button transparent onPress={this.onSearchPressed.bind(this)}>
                        <Text>Search</Text>
                    </Button>
                </Header>

                <Content style={Style.content}>
                    {
                        this.renderContent()
                    }
                </Content>
            </Container>
        )
    }
}

export default SearchPage;