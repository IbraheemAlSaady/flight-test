import { StyleSheet } from 'react-native';
import Theme from '../../config/theme';

export default Style = StyleSheet.create({
    searchInput: {
        fontFamily: 'roboto-regular'
    },
    content: {
        marginTop: 20
    },
    emptyMessage: {
        alignSelf: 'center',
        fontSize: 20,
        fontFamily: 'roboto-medium',
    },
    flightItemContainer: {
        padding: 12,
    },
    destinationContainer: {
        flexDirection: 'row',
        marginTop: 8,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    airlineName: {
        fontFamily: 'roboto-medium',
        color: Theme.primary,
        marginTop: 8
    },
    schedule: {
        fontFamily: 'roboto-regular',
        fontSize: 12,
        color: Theme.grey1,
        marginTop: 8
    },
    separator: {
        height: 1,
        backgroundColor: Theme.grey1
    },
    city: {
        fontFamily: 'roboto-medium',
        fontSize: 14,
        color: Theme.grey1,
        paddingRight: 4
    },
    destinationArrow: {
        color: Theme.grey1,
        fontSize: 14
    }
});