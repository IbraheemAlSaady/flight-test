var baseUrl = 'http://13.82.145.37:9900/search?text=';
// var baseUrl = 'http://192.168.1.12:9900/search?text=';

const get = (text) => {
    return new Promise((resolve, reject) => {
        fetch(baseUrl + text, {
            headers: { 'content-type': 'application/json' },
        }).then(res => res.json()).then(resolve).catch(reject)
    });
};

export { get };