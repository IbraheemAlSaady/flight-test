import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Root } from "native-base";
import SearchPage from './app/components/search';

export default class flight_search_mobile extends Component {
  render() {
    return (
      <Root>
        <SearchPage />
      </Root>
    );
  }
}

AppRegistry.registerComponent('flight_search_mobile', () => flight_search_mobile);
