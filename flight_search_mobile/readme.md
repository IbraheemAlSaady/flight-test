# How to run the app
Running the app

## Getting Started
do `react-native run-ios` or `react-native run-android` to run the app, 
in the `app/services/api` file, there is a base url, where you have to put the server url
for that.

`node 6.9.1` is the node version, if you have nvm, then you can just do `nvm use` on the project root 

```sh
nvm use
npm install
```

do `npm start` on a different window to run the packager, and on another window you can run the  commands `react-native run-ios` or `react-native run-android` for running an ios simulator or android simulator.