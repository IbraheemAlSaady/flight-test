'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var fs = require('fs');
var semver = require('semver');

// Scripts written for the application
var appScripts = [
  'public/javascripts/app.js',
];

// Scripts provided for by vendors
var vendorScripts = [
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
  'node_modules/ejs/ejs.min.js'
];

gulp.task('scss', function () {
  return gulp.src(['public/stylesheets/**/*.scss', '!public/stylesheets/**/_*.scss'])
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('public/stylesheets/'));
});

gulp.task('styles', ['scss'], function() {
  return gulp.src('public/stylesheets/**/*.css')
    .pipe(plugins.cssmin())
    .pipe(plugins.concat('styles.min.css'))
    .pipe(gulp.dest('public/dist/'))
    .pipe(plugins.livereload());
});

gulp.task('lint', function() {
  return gulp.src(['public/**/*.js'])
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format())
      .pipe(plugins.eslint.failAfterError())
      .pipe(plugins.livereload());
});

gulp.task('scripts:app', ['lint'], function() {
  return gulp.src(appScripts)
    .pipe(plugins.concat('app.min.js'))
    .pipe(plugins.uglify())
    .pipe(gulp.dest('public/dist/'))
    .pipe(plugins.livereload());
});

gulp.task('scripts:vendors', function() {
  return gulp.src(vendorScripts)
    .pipe(plugins.concat('vendors.min.js'))
    .pipe(plugins.uglify())
    .pipe(gulp.dest('public/dist/'));
});

gulp.task('fonts', function() {
  return gulp.src([
    'node_modules/font-awesome/fonts/**/*',
    'node_modules/bootstrap-sass/assets/fonts/bootstrap/**/*',
    './public/fonts/**/*',
  ]).pipe(gulp.dest('public/dist/fonts'));
});

// Loads the package for parsing
function getPackageJson() {
  return JSON.parse(fs.readFileSync('./package.json', 'utf8'));
}

// Increments the version using semver
function inc(type) {
  var pkg = getPackageJson();
  var newVer = semver.inc(pkg.version, type);

  return gulp.src(['./package.json'])
    .pipe(plugins.bump({ version: newVer }))
    .pipe(gulp.dest('./'))
    .pipe(plugins.git.commit('Tagged new version [' + newVer + ']'))
    .pipe(plugins.filter('package.json'))
    .pipe(plugins.tagVersion());
}

gulp.task('bump:major', function() { return inc('major'); });
gulp.task('bump:minor', function() { return inc('minor'); });
gulp.task('bump:patch', function() { return inc('patch'); });
gulp.task('bump', ['bump:patch']);

gulp.task('watch', ['build'], function() {
  plugins.livereload.listen();

  // Build the styles
  gulp.watch('public/stylesheets/**/*.scss', ['styles']);

  // Build the app js
  gulp.watch('public/javascripts/**/*.js', ['scripts:app']);
});

gulp.task('build', ['scripts:app', 'scripts:vendors', 'styles', 'fonts']);

gulp.task('default', ['watch']);
