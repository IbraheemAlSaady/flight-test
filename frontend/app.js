var express         = require('express');
var bodyParser      = require('body-parser');
var path            = require('path');
var app             = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

var routesIndex    = require('./routes/index');
app.use('/', routesIndex);

module.exports = app;
