(function () {
  'use strict';

  function addFlightRowItem(item) {
    var template = '<div class="row"><div class="col-md-12"><h1>' + item.fullName + ' (' + item.airlineCode + ') </h1></div></div>'
    return template;
  }

  $(function () {
    // jQuery ready
    $('.js-search-btn').click(function (e) {
      e.preventDefault();
      if (!$('.js-search-input').val()) return alert('type something in the search field');

      $('.js-flights-result').html('<h1>Loading...</h1>');

      $.get('http://13.82.145.37:9900/search?text=' + $('.js-search-input').val(), function (data) {
        $('.js-flights-result').html('');
        
        data.forEach(function (item) {
          $('.js-flights-result').append(addFlightRowItem(item));
        });
      });
    });
  });
})();
