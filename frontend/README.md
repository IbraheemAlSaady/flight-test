# How to run Website
Running the website

## Getting Started
The site by default runs on port 3000, however, the project is setup to be used with dotenv.

`node 6.9.1` is the node version, if you have nvm, then you can just do `nvm use` on the project root 

```sh
nvm use
npm install
npm start
```

on a different window, run `gulp` to run generate the js and css min files.
if you don't have gulp cli, install it globally `npm install -g gulp-cli`